package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public final class Enemigos {
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private Image imagen;
	private int velocidad;

	public Enemigos(int x, int y, int alto, int ancho) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.imagen = Herramientas.cargarImagen("enemigo3.gif");
	this.velocidad = 0;
	}

	public void dibujar(Entorno e) {

		e.dibujarImagen(imagen, this.x, this.y, 0);
		

	}

	public void mover() {
		this.x -= 2- velocidad;

	}

	public void moverderecha() {
		
		this.x += 2;
	}

	// public void moverarriba() {
	// this.y -= 1;
	// }

	// public void moverabajo() {
	// this.y += 1;
	// }

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getAlto() {
		return alto;
	}

	public int getAncho() {
		return ancho;
	}

	public void setX(int x) {
		this.x = x;
	}
	public void setV(int x) {
		this.velocidad = x;
	}

	public void setY(int i) {
		this.y= y;
		// TODO Auto-generated method stub
		
	}


}
