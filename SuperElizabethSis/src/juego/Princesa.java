package juego;

import java.awt.Color;
import java.awt.Image;

import javax.sound.sampled.Clip;

import entorno.Entorno;
import entorno.Herramientas;

public final class Princesa {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private Image imagen;
	private Clip jump;
	private Clip death;
	private Clip letsGo;
	private Clip disparo;
	private int velocidad;
	// BOOLEANOS SALTO Y CAIDA//
		private boolean caida = false;
		private boolean salto = false;
		//Inmunidad//
		private boolean fantasma=true;

	public Princesa(double x, double y, double alto, double ancho) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.imagen = Herramientas.cargarImagen("princesaanimada.gif");
		this.jump = Herramientas.cargarSonido("Jump.wav");
		this.death = Herramientas.cargarSonido("lost_a_live.wav");
		this.letsGo = Herramientas.cargarSonido("LetsGo.wav");
		this.disparo = Herramientas.cargarSonido("disparo.wav");
	}

	// DIBUJAR
	public void dibujar(Entorno e) {
		
		e.dibujarImagen(imagen, this.x, this.y, 0);

	}

	// MOVIMIENTOS//
	public void moverizquierda() {
		this.x -= 2;
		this.imagen = Herramientas.cargarImagen("princesa_izq.gif");

	}

	public void moverderecha() {
		this.x += 2;
		this.imagen = Herramientas.cargarImagen("princesaanimada.gif");
	}
	public void moverderechafantasma() {
		this.x += 1;
		this.imagen = Herramientas.cargarImagen("princesamuere.gif");
	}

	public void moverarriba() {
		this.imagen = Herramientas.cargarImagen("princesasalta.gif");
		this.jump = Herramientas.cargarSonido("Jump.wav");
		this.y -= 4;
		System.out.println(this.y);

	}

	public void moverabajo() {
		this.y += 4;
		this.imagen = Herramientas.cargarImagen("princesasalta.gif");

	}
	public void inmunidad() {
		this.x=30;
		this.imagen = Herramientas.cargarImagen("muere.gif");
	}

	public void perdervida() {
		this.imagen = Herramientas.cargarImagen("muere.gif");
		this.death = Herramientas.cargarSonido("lost_a_live.wav");
		
	}

	public static boolean perdervida;

	// getters//
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAlto() {
		return alto;
	}

	public double getAncho() {
		return ancho;
	}
	public Clip getJump() {
		return jump;
	}

	public Clip getDeath() {
		return death;
	}

	public Clip getLetsGo() {
		return letsGo;
	}

	public Clip getDisparo() {
		return disparo;
	}
	

	// NUEVA BOLA// 
	Bola Lanzada() {
		return new Bola(this.x + 6, this.y + 9, 10);
	}

	public boolean isCaida() {
		return caida;
	}

	public void setCaida(boolean caida) {
		this.caida = caida;
	}

	public boolean isSalto() {
		return salto;
	}

	public void setSalto(boolean salto) {
		this.salto = salto;
	}
	public boolean esFantasma() {
		return fantasma;
	}

	public void setFantasma(boolean fantasma) {
		this.fantasma= fantasma;
	}

	

}
