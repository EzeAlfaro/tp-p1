package juego;

import java.awt.Color;
import java.awt.Image;
import java.util.ArrayList;// SE SUMA
import java.util.Random;



import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {
	// 
	private Entorno entorno;
// Variables y metodos propios de cada grupo
	Random rand =new Random();
	int obs = 3;
	int malos = 3;
	int totalMon = 3;
	int vidas;
	int puntaje;
	Fondo fondo;
	Montana[] montana;
	Princesa princesa;
	Nube[] nube;
	Piso[] piso;
	Bola bola;
	
	Arbustos[] arbustos;
	//Enemigos
	private ArrayList  <Enemigos>   enemigos     = new ArrayList<Enemigos>();
	//Obstaculos
	private ArrayList  <Obstaculos> obstaculos   = new ArrayList<Obstaculos>();
	//Moneda
	private ArrayList  <Moneda>     monedas        = new ArrayList<Moneda>();		
	Image imagen;
	
	int nivel = 1;
	int tiempo = 0;
	int tick = 0;
	boolean contadorTicks;


	Juego() {
		// Inicializa el objeto entorno

		this.bola = null;

		this.entorno = new Entorno(this, "Super Elizabeth Sis - Grupo - v6", 800, 600);

		// Inicializar lo que haga falta para el juego
		// ...

		this.princesa = new Princesa(150, 384, 90, 70);
		this.bola = new Bola(princesa.getX(), princesa.getY(), 20);
		this.fondo = new Fondo(400, 300, 600, 800);

		this.piso = new Piso[100];
		for (int i = 0; i < this.piso.length; i++) {
			this.piso[i] = new Piso(500 + (600 * i), 500, 90, 70);
		}

//MONEDA
		for (int i = 0; i < totalMon; i++) {
			Moneda moneda = new Moneda(500 + (700 * i)+rand.nextInt(70), 50+rand.nextInt(100), 30, 30);
			monedas.add(moneda);
		}

		this.arbustos = new Arbustos[100];
		for (int i = 0; i < this.arbustos.length; i++) {
			this.arbustos[i] = new Arbustos(200 + (400 * i), 360, 90, 70);
		}
		this.vidas=3;

		
		
// obstaculos ( suma elemntos al array)
		
		for(int i  = 0; i < obs; i ++) {
			 Obstaculos  obstaculo = new Obstaculos(700 + (700 * i), 380, 90, 70);	
			obstaculos.add(obstaculo); 
				}
		
// ENEMIGOS ( suma elemntos al array)
		for(int i  = 0; i < malos; i ++) {
			 Enemigos  enemigo = new Enemigos(700 + (400 * i)+rand.nextInt(300), 400, 90, 70);	
			 enemigos.add(enemigo); 
				}
		//Enemigos  enemigo1 = new Enemigos(700 + (400 )+rand.nextInt(300), 400, 90, 70);	
		

		this.nube = new Nube[2];
		for (int i = 0; i < this.nube.length; i++) {
			this.nube[i] = new Nube(300 + (650 * i), 50, 50, 70);
		}

		this.montana = new Montana[3];
		for (int i = 0; i < this.montana.length; i++) {
			this.montana[i] = new Montana(300 + (800 * i), 250, 50, 70, princesa.getX());
		}
			
		
		// Inicia el juego!
		this.entorno.iniciar();
		princesa.getLetsGo().start();
	}

	// metodo entre princesa y enemigo//
	boolean colisionPrincesaEnemigo(Enemigos enemigo) {
		if (this.princesa.getY() + this.princesa.getAlto() / 2 > enemigo.getY() - enemigo.getAlto() / 2
				&& this.princesa.getX() + this.princesa.getAncho() / 2 > enemigo.getX() - enemigo.getAncho() / 2
				&& this.princesa.getY() - this.princesa.getY() / 2 < enemigo.getY() + enemigo.getAlto() / 2
				&& this.princesa.getX() - this.princesa.getAncho() / 2 < enemigo.getX() + enemigo.getAncho() / 2)
			return true;
		else
			return false;
	}

	// metodo colision entre princesa y obstaculo//
	boolean colisionPrincesaObst(Obstaculos obs) {
		if (this.princesa.getY() + this.princesa.getAlto() / 2 > obs.getY() - obs.getAlto() / 2
				&& this.princesa.getX() + this.princesa.getAncho() / 2 > obs.getX() - obs.getAncho() / 2
				&& this.princesa.getY() - this.princesa.getY() / 2 < obs.getY() + obs.getAlto() / 2
				&& this.princesa.getX() - this.princesa.getAncho() / 2 < obs.getX() + obs.getAncho() / 2)

			return true;
		else

			return false;
	}
    //metodo colision entre Princesa y moneda
	boolean colisionPrincesaMoneda(Moneda mon) {
		if (this.princesa.getY() + this.princesa.getAlto() / 2 > mon.getY() - mon.getAlto() / 2
				&& this.princesa.getX() + this.princesa.getAncho() / 2 > mon.getX() - mon.getAncho() / 2
				&& this.princesa.getY() - this.princesa.getY() / 2 < mon.getY() + mon.getAlto() / 2
				&& this.princesa.getX() - this.princesa.getAncho() / 2 < mon.getX() + mon.getAncho() / 2)

			return true;
		else

			return false;
	}

void setnivel(int tiempo) {
if (tiempo%1000==0&& tiempo<4000) {
	 nivel += 1;
	 System.out.println("nivel" + nivel);
}
	
}
	/**
	 * Durante el juego, el mtodo tick() ser ejecutado en cada instante y por lo
	 * tanto es el mtodo ms importante de esta clase. Aqu se debe actualizar el
	 * estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */

	// *****************TICKS INSTANTE DE TIEMPO***********************//

	public void tick() {
		try {
		if(puntaje>=70)
			
		{   entorno.dibujarImagen(Herramientas.cargarImagen("youWin.png"), (double)30, (double)30, (double)0);
			this.princesa.getLetsGo();
			return;
		}
		 if(vidas<=0) 
		{	
			 entorno.dibujarImagen(Herramientas.cargarImagen("theEnd.png"), (double)30, (double)30, (double)0);
			
			return;
		}
		 
		 
		}
		 catch(Exception e) {
		}
	
// CONTADOR DE TICKS//
		tiempo=tiempo+1;
		
		if (contadorTicks == true) {
			tick = tick + 1;
		}
		
	

//VELOCIDAD//
		setnivel(tiempo);
		
		
// FONDO 

// DIBUJA FONDO//
		this.fondo.dibujar(this.entorno);

//MONTANAS// DIBUJA Y MUEVE MONTAAS//

		for (int i = 0; i < this.montana.length; i++) {
			this.montana[i].mover();
			this.montana[i].dibujar(this.entorno);
			if (this.montana[i].getx() <= -300) {
				this.montana[i].setX(1500);
			}
			// podemos crear un bucle
		}

//PISO		// DIBUJA PISO //


		for (int i = 0; i < this.piso.length; i++) {
			this.piso[i].mover();
			this.piso[i].setV(nivel);
			this.piso[i].dibujar(this.entorno);
			if (this.piso[i].getx() <= -50) {
				this.piso[i].setX(1050);
			}
		}

		// Procesamiento de un instante de tiempo
		

//PRINCESA		// MOVIENTO SOBRE EJE X DE PRINCESA//
		if (this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA) && princesa.getX() > 20) {

			this.princesa.moverizquierda();
		}

		if (this.entorno.estaPresionada(this.entorno.TECLA_DERECHA) && princesa.getX() < this.entorno.ancho() - 450 ) {
			this.princesa.moverderecha();
			
					}

// SALTO PRINCESA//
		if (this.entorno.estaPresionada(this.entorno.TECLA_ARRIBA) && princesa.isCaida() == false ) {
			princesa.setSalto(true);

		}

		if (princesa.isCaida() == true) {
			this.princesa.moverabajo();
			if (princesa.getY() == 380) {
				this.princesa.moverderecha();
				princesa.setCaida(false);
			}

		}

		if (princesa.isSalto() == true) {
			this.princesa.moverarriba();
			if (princesa.getY() == 376) {
				princesa.getJump().start();
			}
			if (princesa.getY() == 124) {
				princesa.setSalto(false);
				princesa.setCaida(true);

			}

		}

		// DIBUJA PRINCESA//
		this.princesa.dibujar(this.entorno);

//OBSTACULO // DIBUJA Y MUEVE OBSTACULOS// falta ciclo infinito
		for (int i = 0; i < this.obstaculos.size(); i++) {
			this.obstaculos.get(i).mover();
			this.obstaculos.get(i).setV(nivel);
			this.obstaculos.get(i).dibujar(this.entorno);
			if (this.obstaculos.get(i).getX() <= -50) {
				this.obstaculos.get(i).setX(2000+rand.nextInt(1000));
				// para cumplir requerimiento obligatorio .4

			}
			// COLISION PRINCESA OBSTACULO//

			if (this.colisionPrincesaObst(this.obstaculos.get(i)) && tick == 0) {

				System.out.println("colision princesa obstaculo");

				this.vidas= vidas -(1);
				this.princesa.perdervida();
				this.princesa.setFantasma(true);
				contadorTicks = true;
				System.out.println(tick);

				}
			
				

			}

			if (tick > 250) {
				contadorTicks = false;
				tick = 0;
				this.princesa.moverderecha();
				System.out.println(tick);
				
			
			}
			if (tick>1) {
				for (int i = 0; i < this.obstaculos.size(); i++) {
					this.obstaculos.get(i).setV(-3);
					for (int j = 0; j < this.enemigos.size(); j++) {
						this.enemigos.get(j).moverderecha();
			}}}
				
		

//ENEMIGOS		// DIBUJA Y MUEVE ENEMIGOS//
			
		for (int i = 0; i < this.enemigos.size(); i++) {
			
			this.enemigos.get(i).setV(-nivel);
			this.enemigos.get(i).mover();
			this.enemigos.get(i).dibujar(this.entorno);
			if (this.enemigos.get(i).getX() <= -10) {
				this.enemigos.remove(i);
				this.enemigos.add(  new Enemigos(1500+rand.nextInt(2000), 400, 90, 70));}//eliminamos de memoria si cruza los 810 pixeles
// importante enemigos se superponen		    

// COLISION PRINCESA-ENEMIGO//
			if (colisionPrincesaEnemigo(this.enemigos.get(i)) && vidas>0&& tick == 0) {
				System.out.println("colision princesa-enemigo");
				this.enemigos.remove(i);
				this.enemigos.add(  new Enemigos(1500+rand.nextInt(2000), 400, 90, 70));
				this.princesa.setFantasma(true);
				this.princesa.perdervida();
				//this.princesa.inmunidad();
				
				princesa.getDeath().start();
				this.vidas=vidas - 1;
				this.princesa.perdervida();
				this.nivel=1;
				contadorTicks = true;
			}

			if (tick > 500) {
				this.princesa.setFantasma(false);
				contadorTicks = false;
				tick = 0;
				this.princesa.moverderecha();
				System.out.println(tick);
			}

// COLISION BOLA-ENEMIGO//

			if (this.enemigos.get(i).getX() <= this.bola.getX() && this.princesa.getX() < this.enemigos.get(i).getX()) {
				if (this.bola.getX() == this.princesa.getX()) {
					this.puntaje=puntaje + 0;
				} else

				{
					System.out.println("bola-enemigo");
					this.bola = new Bola(princesa.getX(), princesa.getY(), 20);
					this.enemigos.remove(i);
					this.enemigos.add(  new Enemigos(700 + (400 )+rand.nextInt(800), 400, 90, 70));
					this.puntaje=puntaje + 5;
				}

			}

		}

//DISPARO		// DIBUJA Y MUEVE BOLA//
		if (this.entorno.estaPresionada(this.entorno.TECLA_ESPACIO)&&this.princesa.isSalto()==false&&princesa.isCaida()==false)

		{
			princesa.getDisparo().start(); // DEBERIA ACORTAR EL TONO PARA QUE SEA UN SOLO DISPARO
			this.bola.dibujar(this.entorno);
			this.bola.setFuego(true);// para que se mantenga dibujada despues de tocar la tecla
			this.bola.mover();
		}
		// llega al limite y se crea una nueva bola
		if (this.bola.getX() > 801) {
			this.bola = new Bola(princesa.getX(), princesa.getY(), 20);

		}
		// disparo se mueve
		if (this.bola.isFuego()) {
			this.bola.dibujar(this.entorno);
			this.bola.mover();

		}

//ARBUSTOS//
		for (int i = 0; i < this.arbustos.length; i++) {
			this.arbustos[i].mover();
			this.arbustos[i].setV(nivel);
			this.arbustos[i].dibujar(this.entorno);
			if(this.princesa.esFantasma()) {
				this.arbustos[i].setStop(0);
				
			}
			else {this.arbustos[i].setStop(0);

			}
		}
//MONEDA//
		for (int i = 0; i < this.monedas.size(); i++) {
			this.monedas.get(i).mover();
			this.monedas.get(i).dibujar(this.entorno);
			if (this.monedas.get(i).getX()<-10) {
				this.monedas.remove(i);
				this.monedas.add(new Moneda (1500 + (700 * i)+rand.nextInt(70), 50+rand.nextInt(100), 30, 30));
				
			}
			if (colisionPrincesaMoneda(this.monedas.get(i))) {
				puntaje = puntaje +1;
				this.monedas.remove(i);
				this.monedas.add(new Moneda (1500 + (700 * i)+rand.nextInt(70), 50+rand.nextInt(100), 30, 30));
			}
		}
//NUBES		// DIBUJA Y MUEVE NUBES//
		for (int i = 0; i < this.nube.length; i++) {
			this.nube[i].mover();
			this.nube[i].dibujar(this.entorno);

		}

//VIDAS		// *********DIBUJA  VIDAS Y PUNTAJE*******************//
		
		entorno.dibujarImagen(Herramientas.cargarImagen("corazon.png"), 50, 60, 0);
		entorno.cambiarFont("Arial", 25, Color.white);
		entorno.escribirTexto(Integer.toString(vidas), 50 + 30, 60+ 7);
//PUNTAJE
		entorno.dibujarImagen(Herramientas.cargarImagen("point.png"), 700, 60, 0);
		entorno.cambiarFont("Arial", 25, Color.white);
		entorno.escribirTexto(Integer.toString(puntaje), 700 + 30, 60 + 7);
//CARTEL PERDISTE VIDA
		if (contadorTicks) {
			entorno.dibujarImagen(Herramientas.cargarImagen("perdistevida.png"), (double)30, (double)30, (double)0);
			
		}
	}
	

//	@SuppressWarnings("unused")
//	public static void main(String[] args) {
//		Juego juego = new Juego();
//	}
}
