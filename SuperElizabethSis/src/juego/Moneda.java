package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Moneda {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private Image imagen;

	public Moneda(double x, double y, double alto, double ancho) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.imagen = Herramientas.cargarImagen("point.png");
	}

	public void dibujar(Entorno e) {
		//e.dibujarRectangulo(x, y, ancho, alto, 0, Color.YELLOW);
		e.dibujarImagen(imagen, this.x, this.y, 0);

	}

	public void mover() {
		this.x -= 2.5;

	}

	public double getY() {
		// TODO Auto-generated method stub
		return y;
	}

	public void setX(int i) {
		this.x = i;
		// TODO Auto-generated method stub

	}

	public double getX() {
		// TODO Auto-generated method stub
		return x;
	}

	public double getAlto() {
		// TODO Auto-generated method stub
		return this.alto;
	}

	public double getAncho() {
		// TODO Auto-generated method stub
		return this.ancho;
	}
}
