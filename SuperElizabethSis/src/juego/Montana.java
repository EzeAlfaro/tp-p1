package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Montana {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private double velocidad;
	private Image imagen;

	public Montana(double x, double y, double alto, double ancho, double velocidad) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.imagen = Herramientas.cargarImagen("pico.png");
	}

	public void dibujar(Entorno e) {

		e.dibujarImagen(imagen, this.x, this.y, 0);

	}

	public void mover() {
		this.x -= 1+velocidad;

	}

	public double getx() {
		// TODO Auto-generated method stub
		return x;
	}

	public void setX(int i) {
		// TODO Auto-generated method stub
		this.x = i;
	}
	public void setV(int i) {
		// TODO Auto-generated method stub
		this.velocidad = i;
	}
}